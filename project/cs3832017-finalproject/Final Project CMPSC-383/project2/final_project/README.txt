Tyler Lyle, Zijun Xia, Jonathan Yee
CMPSC 383
Janyl Jumadinova
Final Project

NOTE: The Follower program that is used in this project was provided by the
ROS wiki library

************************************

These programs imitate the behavior of a predator and prey organisms.
The turtlebot predator chases the prey while the legobot prey runs away
from anything it senses in its ultrasonic sensors.

************************************
HOW TO RUN
************************************
************************************
Turtlebot
************************************

You can either access this program from a remote computer or the actual
turtlebot laptop. Make sure you have access to the Bash files. 

In the folder: "cs383finalproject", open the terminal.

To gain permission for Bash files, use the command: "chmod +x <path/to/file>"

Then you can type in the command as shown: ./Predator.sh

This bash file runs a script which starts the turtlebot's ROS functions
and then starts the follower program included in the other folder.

************************************
Lego Robot
************************************

If the program FinalRunner.java is not already in the robot, either connect the robot
into the computer via USB and download it using the program ECLIPSE or 
you can SSH into the robot via the terminal.

Once the program is in the robot, you can use choose to run it from the 

************************************
SET-UP
************************************

This program works best in a hallway or an open area that's free from obstructions.

Place the lego robot on the floor with the program loaded in and ready to
start.

Place the turtlebot, also ready to run its program behind it. 

Start the programs simultaneously on each robot.

The programs will end when the lego robot and turtlebot have completely 
stopped moving.

To stop the turtlebot, you can press CTRL+C on the laptop's keyboard.
