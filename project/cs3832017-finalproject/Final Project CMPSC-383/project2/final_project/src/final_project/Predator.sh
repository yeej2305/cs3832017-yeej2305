#!/bin/bash

clear

echo "Turtlebot Startup..."

sleep 5s

gnome-terminal -e "roscore"

#sleeps for 4 seconds
sleep 4s
#starts up a new terminal with command:
gnome-terminal -e "roslaunch turtlebot_bringup minimal.launch"

#same
sleep 4s
# ---
gnome-terminal -e "roslaunch turtlebot_follower follower.launch"


