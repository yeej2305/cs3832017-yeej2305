package prey;

import java.util.Random;

import lejos.hardware.Brick;
import lejos.hardware.Button;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.utility.Delay;
import lejos.utility.PilotProps;

/**
*
*
*
*
*
*
*
*
*/

public class Run_Away {

	public static void main(String[] args) {
		
		//create new differential pilot
		PilotProps p= new PilotProps();
		RegulatedMotor leftMotor = PilotProps.getMotor(p.getProperty(PilotProps.KEY_LEFTMOTOR, "B"));
		RegulatedMotor rightMotor = PilotProps.getMotor(p.getProperty(PilotProps.KEY_RIGHTMOTOR, "C"));
	
    	float TYRE_DIAMETER = 5.6f;
    	final float AXLE_TRACK = 9.5f;
		final DifferentialPilot pilot = new DifferentialPilot(TYRE_DIAMETER, AXLE_TRACK, leftMotor, rightMotor);
		
		// Change Speed Here //
		pilot.setAcceleration(200);
		
		// set up a sonic sensor
		EV3UltrasonicSensor sonicSensor1 = new EV3UltrasonicSensor(SensorPort.S1);
		SampleProvider sonicSamplePr1 = sonicSensor1.getDistanceMode();
		float[] sonicSample1 = new float[sonicSamplePr1.sampleSize()];

		EV3UltrasonicSensor sonicSensor2 = new EV3UltrasonicSensor(SensorPort.S2);
		SampleProvider sonicSamplePr2 = sonicSensor2.getDistanceMode();
		float[] sonicSample2 = new float[sonicSamplePr2.sampleSize()];
	
		EV3UltrasonicSensor sonicSensor3 = new EV3UltrasonicSensor(SensorPort.S3);
		SampleProvider sonicSamplePr3 = sonicSensor3.getDistanceMode();
		float[] sonicSample3 = new float[sonicSamplePr3.sampleSize()];

		
		while(true)
		{
			//check for objects
			pilot.rotate(360);
			
			sonicSamplePr1.fetchSample(sonicSample1, 0);
			sonicSamplePr2.fetchSample(sonicSample2, 0);
			sonicSamplePr2.fetchSample(sonicSample3, 0);
			if(sonicSample1[0]<0.1 || sonicSample2[0]<0.1 || sonicSample3[0]<0.1)  
			{
				pilot.arcForward(60);
			}//if
			//Change forward distance//
			pilot.travel(50);
			
			
		}//while
		
	}//main
	
	public class RandomBehavior{
		
	}//RandomBehavior
	
}//class Run_Away
