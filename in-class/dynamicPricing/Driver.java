import java.io.*;

public class Driver{
    
    public static void main(String [] args){
        
        Market m = new Market();
        
        // 500 intervals * 20 quotes/interval=10,000 quotes / 50 sellers = 200 quotes/buyer
        // 200 quotes/buyer * 10 seconds/quote = 2000 seconds to run the algorithm
        
	int period = 2001;
        
        for(int i=0; i<period; i++ ){
            if ( (i%10) == 0 ){
                m.operate();
            }
        }
        
        //file output
        try{
            File data = new File("output.csv");
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(data)));
            for(int i=0; i<m.sellers.length; i++ ){
                out.println("Seller "+i+":\n "+m.sellers[i].history);
            }
            out.close();
        }catch(Exception e){}
        
        System.exit(0);
    }
}