public class Market{
    public Buyer  [] buyers ;
    public Seller [] sellers;
    
    public Market(){
        //create market
        buyers  = new Buyer [200];
        sellers = new Seller[2];
        
        //instantiate buyers
        for (int i = 0; i < buyers.length; i++){
            buyers[i] = new Buyer(this);
        }
        
        //instantiate sellers
        int half = sellers.length / 2;
        for (int i = 0; i < sellers.length; i++){
            if ( i < half ){
                sellers[i] = new Seller(this,1);    //random-pricing sellers
            }else{
                sellers[i] = new Seller(this,2);    //hill-climing sellers
            }
        }
    }
    
    void operate(){
        //call buy() for each buyer sequentially
        for (int i = 0; i < buyers.length; i++){
            buyers[i].buy();
        }
    }
}
