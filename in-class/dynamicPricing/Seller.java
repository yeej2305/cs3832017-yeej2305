import java.util.Vector;

public class Seller{
    
    private double revenue;
    private int numQuotes;
    private int numUnitsSold;
    private int timeInterval;
    private double currentPrice;
    private final double baseCost = 20.0;
    
    Vector<History> history = new Vector<History>();
    int type;
    
    public Seller(Market m, int t){
        revenue      = 0;
        numQuotes    = 0;
        numUnitsSold = 0;
        timeInterval = 0;
        type = t;
        double tempPrice;
        if ( type == 1 ){
            do {
                tempPrice = Math.random()*100;
            }while ( tempPrice < baseCost );
            currentPrice = tempPrice;
        }

    }
    
    double requestPrice(){
        if ( numQuotes == 20 ){
            this.adjustPrice();
        }
        numQuotes++;
        return currentPrice;
    }
    
    void selectSeller(){
        revenue += currentPrice;
        numUnitsSold++;
    }
    
    private void adjustPrice(){
        double profit = revenue - numUnitsSold*baseCost;
        History newest = new History(profit, currentPrice, timeInterval);
        history.add(newest);
        
        //change currentPrice
        double tempPrice;
        if ( this.type == 1 ){
            do {
                tempPrice = Math.random()*100;
            }while ( tempPrice < baseCost );
            this.currentPrice = tempPrice;
        }
        else{
            if ( history.size()<=20 ){  //do random pricing in the beginning
                do {
                    tempPrice = Math.random()*100;
                }while ( tempPrice < baseCost );
                this.currentPrice = tempPrice;
            }else{
                int bigT = ((History)(history.elementAt(history.size()-1))).timeIntervalId; //most recent time
                History current = (History)(history.elementAt(0));
                int smallT = current.timeIntervalId;
                double adjustedProfit = current.profit * Math.exp(smallT-bigT);
                double bestProfit = adjustedProfit;
                double bestPrice = current.currentPrice;
                for(int i=1; i<history.size(); i++){
                    current = (History)(history.elementAt(i));
                    smallT = current.timeIntervalId;
                    adjustedProfit = current.profit * Math.exp(smallT-bigT);
                    if ( adjustedProfit > bestProfit ){
                        bestProfit = adjustedProfit;
                        bestPrice = current.currentPrice;
                    }
                }
                this.currentPrice = bestPrice;
            }
        }
        
        
        revenue      = 0;
        numQuotes    = 0;
        numUnitsSold = 0;
        timeInterval++;
    }
}