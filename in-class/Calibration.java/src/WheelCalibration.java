import lejos.hardware.motor.Motor;
import lejos.robotics.navigation.DifferentialPilot;

public class WheelCalibration {
	DifferentialPilot pilot;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new WheelCalibration();
	}
	public WheelCalibration(){
		
		// Differential Pilot (Diameter, track width, left motor, right motor)
		pilot = new DifferentialPilot(5.715f, 12.065f, Motor.C, Motor.B);
		pilot.travel(100);
	}
}
