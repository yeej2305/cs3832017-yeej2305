package GyroSensor;

import java.io.IOException;
import java.io.*;

import lejos.hardware.sensor.EV3GyroSensor;
import lejos.hardware.sensor.EV3TouchSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import java.io.IOException;
import lejos.hardware.Button;
import lejos.hardware.lcd.LCD;
import lejos.hardware.port.SensorPort;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;
import lejos.utility.TextMenu;
import lejos.hardware.ev3.LocalEV3;
import lejos.hardware.port.Port;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.hardware.sensor.NXTLightSensor;
import lejos.hardware.Sound;
import lejos.hardware.motor.*;
import lejos.robotics.navigation.DifferentialPilot;


public class Navigation{
	public static Port portG = LocalEV3.get().getPort(portMenuGyro());
	public static final EV3GyroSensor gyroSensor = new EV3GyroSensor(portG);
	public static final double NORTH = 0.0;
	public static final double WEST = 90.0;
	public static final double SOUTH = 180.0;
	public static final double EAST = 270.0;
	public static final double ERROR = 30;
	
	public static void main(String [] robot) throws InterruptedException{
		SampleProvider sample = gyroSensor.getAngleMode();
		float [] values = new float[sample.sampleSize()];
		sample.fetchSample(values, 0);
		double angle = 0;
		while(Button.ESCAPE.isUp()){
			sample.fetchSample(values,0);
			angle = values[0];
			System.out.println("angle: "+angle);
			if(angle < 0) angle = -angle;
			LCD.drawString("" + (angle % 360), 0,1);
			//if(360 % (angle + ERROR) <= (NORTH + ERROR) && 360 % (angle - ERROR) >= (NORTH - ERROR))
			check(angle);
			randomTurn();
			//check(angle);
			
			Thread.sleep(4000);
			LCD.clear();
		}
		gyroSensor.close();
			
	}
	
	private static String portMenuGyro(){
		String ports[] = {"Port 1", "Port 2", "Port 3", "Port 4"};
    		TextMenu portMenu = new TextMenu(ports, 1, "Gyro port");
    		int number = portMenu.select() + 1 ;
    		LCD.clear();
    		return "S" + number;
	}
	
	private static void randomTurn(){
		DifferentialPilot pilot = new DifferentialPilot(4.0,14.0,Motor.B, Motor.C);
		int random  = (int)(5*Math.random());
		
		switch(random){
			case 1:
				random = 90;
				break;
			case 2:
				random = 180;
				break;
			case 3:
				random = 270;
				break;
			case 4:
				random = 360;
				break;
			default:
				random = 90;
				break;
		}
		int randomSign = (int)(3*Math.random());
		if(randomSign == 1) random = -random;
		
		pilot.rotate(random);
	}
	
	private static void check(double angle){
		LCD.clear();
		if(angle % 360 <= (NORTH + ERROR) && angle % 360 >= (NORTH - ERROR))
			LCD.drawString("I'm pointing NORTH",0,4);
		else if(angle % 360 <= (WEST + ERROR) && angle % 360 >= (WEST - ERROR))
			LCD.drawString("I'm pointing WEST",0,4);
		else if(angle % 360 <= (SOUTH + ERROR) && angle % 360 >= (SOUTH - ERROR))
			LCD.drawString("I'm pointing SOUTH",0,4);
		else if(angle % 360 <= (EAST + ERROR) && angle % 360 >= (EAST - ERROR))
			LCD.drawString("I'm pointing EAST",0,4);
		else
			LCD.drawString("I'm Lost :(",0,4);
	}
}