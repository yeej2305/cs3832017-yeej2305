import lejos.hardware.Brick;
import lejos.hardware.BrickFinder;
import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.utility.Delay;

/**
* This program runs on a differential robot that has 55mm diameter wheels and a robot track of 115.5mm.
* This robot will travel straight and then rotate 90 degrees.
*
* It is done using the DifferentialPilot class.
*
* @author jjumadinova
*
* TO DO: Modify the existing code to try the following:
* - obtain a speed from one motor, rotate one motor only.
* - What happens if two motors operate at different speeds?
* - Is there any difference in using motor's stop() method vs. DifferentialPilot's?
*/
public class Motors {
	 
    public static void main(String[] args) {
    	
    	// getDefault() uses the local EV3 brick
        Brick brick=BrickFinder.getDefault();
	         
        RegulatedMotor leftMotor=new EV3LargeRegulatedMotor(brick.getPort("B"));
        RegulatedMotor rightMotor=new EV3LargeRegulatedMotor(brick.getPort("C"));
	    
        // connect the motors to a pilot
        DifferentialPilot pilot=new DifferentialPilot(5.5, 11.55, leftMotor, rightMotor);
        
        System.out.println("Push a button to start the movement!");
	    Button.waitForAnyPress();
	    
	    // change the speed
	    pilot.setTravelSpeed(10);
	    
	    // travel forward
	    pilot.travel(50);
	    
	    //wait
	    Delay.msDelay(100);
	    
	    // rotate
	    pilot.rotate(90);
	    
	    // stop
	    pilot.stop();
	    
    }

}