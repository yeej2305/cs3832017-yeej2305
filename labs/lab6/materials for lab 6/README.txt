**** START LAB ****
***
**
*

*
**
***
**** OVERALL SOLUTION ****

-Have one turtlebot be able to follow another using the follower program.

**** OVER ALL SOLUTION ****
***
**
*


*
**
***
**** HOW TO SET UP THE LAB ****

- To gain permission for Bash files, use the command chmod +x <path/to/file>

- Then you can type in the command as shown: ./<name of file>

 - On the remote PC, run RemoteComputer.sh
	-brings up the keyboard teleop

 - Run Turtlebot2.sh
 	- this will open up rviz with the map data


**** HOW TO SET UP THE LAB ****
***
**
*

*
**
***
**** END LAB ****
