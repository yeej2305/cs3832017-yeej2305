package Lab2;

import lejos.hardware.Brick;
import lejos.hardware.Button;
import lejos.hardware.Sound;
import lejos.hardware.port.MotorPort;
import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3ColorSensor;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.Color;
import lejos.robotics.RegulatedMotor;
import lejos.robotics.SampleProvider;
import lejos.robotics.navigation.DifferentialPilot;
import lejos.utility.PilotProps;
import lejos.hardware.motor.EV3LargeRegulatedMotor;
import lejos.hardware.motor.Motor;


public class balloon_maze {

	static final float MAX_DISTANCE = 5f; // max distance that is returned for the sensor
    static final int DETECTOR_DELAY = 200; // min delay between notifications from sensor readings

	public static void main(String[] args) {
		
		// Create a configuration object for the Differential Pilot
		PilotProps p= new PilotProps();	
		
		//left and right wheel motors
		RegulatedMotor leftMotor = PilotProps.getMotor(p.getProperty(PilotProps.KEY_LEFTMOTOR, "B"));
		System.out.println("Motor B is working");
		RegulatedMotor rightMotor = PilotProps.getMotor(p.getProperty(PilotProps.KEY_RIGHTMOTOR, "C"));
		System.out.println("Motor C is working");
		
    	float TYRE_DIAMETER = 5.6f;
    	final float AXLE_TRACK = 9.5f;
		final DifferentialPilot pilot = new DifferentialPilot(TYRE_DIAMETER, AXLE_TRACK, leftMotor, rightMotor);
		
		// *** CHANGE the SPEED here ***//
		pilot.setAcceleration(50);
		
		EV3ColorSensor colorSensor = new EV3ColorSensor(SensorPort.S2);
		SampleProvider colorSamplePr = colorSensor.getRedMode();
		float[] colorSample = new float[colorSamplePr.sampleSize()];
		
		// set up a sonic sensor
		EV3UltrasonicSensor sonicSensor = new EV3UltrasonicSensor(SensorPort.S1);
		SampleProvider sonicSamplePr = sonicSensor.getDistanceMode();
		float[] sonicSample = new float[sonicSamplePr.sampleSize()];
		System.out.println("Get in the robot Shinji!");
		Button.waitForAnyPress();
		
		sonicSamplePr.fetchSample(sonicSample, 0);
		System.out.println("Distance is "+sonicSample[0]);
		
		boolean turnRight = true;
		
		while(true)
		{
			//*** CHANGE the FORWARD TRAVEL DISTANCE here ***//
			pilot.travel(-(sonicSample[0]*100)-1);
			
			// check for objects
			sonicSamplePr.fetchSample(sonicSample, 0);
			if(sonicSample[0]<0.15)
			{
				// Perform a movement to avoid the obstacle.
				//*** CHANGE the BACKUP TRAVEL DISTANCE here ***//
				double num = 5;
				pilot.travel(num);
				System.out.println("moving back d is: "+num);

				//if we encounter a box turn right
				if (turnRight == true) {
					pilot.rotate(90);
					turnRight = false;
				}
				//if we encounter a box turn left
				else {
					pilot.rotate(-90);
					turnRight = true;
				}

				sonicSamplePr.fetchSample(sonicSample, 0);
				if(sonicSample[0]<0.3) {
						pilot.rotate(180);
				}
				
				//travel to the next obstacle
				pilot.travel(-(sonicSample[0]*100)-1);
				
				// check the color of the balloon
				colorSamplePr.fetchSample(colorSample, 0);
				// if reached the end (red finish line) - stop			
				if(colorSample[0]>=.12) {
					Motor.A.setAcceleration(50000);
					Motor.A.rotate(180);
					Motor.A.rotate(-180);
					System.out.println("Found a red ballon, but I have no arms :(");
				}
				
				// if we made a right turn, change angle to forward
				if (turnRight == false) {
					pilot.rotate(-90);
				}
				//if we just made a left turn, change angle to forward
				else {
					pilot.rotate(90);
				}
				
			}
			

		}
	}
	
}
