package Lab2;

import lejos.hardware.port.SensorPort;
import lejos.hardware.sensor.EV3UltrasonicSensor;
import lejos.robotics.SampleProvider;
import lejos.utility.Delay;

public class distance_Test {

	public static void main(String[] args) {
		EV3UltrasonicSensor sonicSensor = new EV3UltrasonicSensor(SensorPort.S1);
		SampleProvider sonicSamplePr = sonicSensor.getDistanceMode();
		float[] sonicSample = new float[sonicSamplePr.sampleSize()];

		
		boolean temp = true;
		
		while (temp) {
			sonicSamplePr.fetchSample(sonicSample, 0);
			
			System.out.println("Distance is "+sonicSample[0]+"Meters /n");
			
			Delay.msDelay(300);
		}

	}
}
